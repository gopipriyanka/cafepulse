import React from 'react'
import Navbar from './Components/Navbar'
import Home from './Components/Home'
import Menu from './Components/Menu'
import About from './Components/About'
import Product from './Components/Product'
import Footer from './Components/Footer'
import Login from './Components/Login'
import Registration from './Components/Register'
import Contact from './Components/Contact'

const App = () => {
  return (
    <div>
      <Navbar/>
      <main>
        <div id='register'>
          <Registration/>
        </div>
        <div id='login'>
        <Login/>
        </div>
        <div id='home'>
          <Home/>
        </div>
        <div id='menu'>
          <Menu/>
        </div>
        <div id='about'>
          <About/>
        </div>
        <div id='product'>
          <Product/>
        </div>
        <div id='contact'>
          <Contact/>
        </div>
      </main>
      <Footer/>
    </div>
  )
}

export default App